module.exports = {
  extends: "google",
  rules: {
    "max-len": [2, 120]
  },
  parserOptions: {
    ecmaVersion: 6
  }
};
