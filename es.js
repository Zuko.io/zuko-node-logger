const util = require('util');
const zukoLogger = require('.');

/**
 * Creates and returns an EsLogger constructor
 * @param {Object} [meta] Default metadata for each message logged
 * @return {Function}
 */
const createConstructor = (meta) => {
  const logger = zukoLogger.createInstance(meta);

  /**
   * A constructor for a logger that wraps around a winston logger, exposing the API
   * required by Elasticsearch
   * @constructor
   */
  return function() {
    this.error = function(e) {
      if (e instanceof Error) {
        if (e.message.match(/Request error, retrying/)) return this.warning(e);
        logger.error(e);
      } else {
        if (typeof e === 'string') {
          e = e.replace(/\n+/g, ' ');
        }
        if ((e.message && e.message.match(/Request error, retrying/)) || (e.match(/Request error, retrying/))) {
          return this.warning(e);
        }
        logger.error(e);
      }
    };
    this.warning = function(...args) {
      args = args.map((arg) => {
        return typeof arg === 'string' ? arg : util.inspect(arg, {breakLength: Infinity});
      }).join(' ');
      logger.warn(args);
    };
    this.info = function(...args) {
      args = args.map((arg) => {
        return typeof arg === 'string' ? arg : util.inspect(arg, {breakLength: Infinity});
      }).join(' ');
      logger.info(args);
    };
    this.debug = function(...args) {
      args = args.map((arg) => {
        return typeof arg === 'string' ? arg : util.inspect(arg, {breakLength: Infinity});
      }).join(' ');
      logger.debug(args);
    };
    this.trace = function(method, requestUrl, body, responseBody, responseStatus) {
      logger.trace(util.inspect({
        method: method,
        requestUrl: requestUrl,
        body: body,
        responseBody: responseBody,
        responseStatus: responseStatus,
      }, {breakLength: Infinity}));
    };

    // We do not need to close the Winston logger but ES requires logger to have a close method
    this.close = function() {};
  };
};

module.exports = {createConstructor};
