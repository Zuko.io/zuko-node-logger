const winston = require('winston');
const DummyTransport = require('./lib/dummy_transport');

// Define a set of levels and colours
const customLevels = {
  levels: {
    critical: 0,
    error: 1,
    warn: 2,
    info: 3,
    verbose: 4,
    debug: 5,
    trace: 6,
  },
  colors: {
    critical: 'bgRed',
    error: 'red',
    warn: 'yellow',
    info: 'green',
    verbose: 'cyan',
    debug: 'blue',
    trace: 'grey',
  },
};

// Override default Winston levels and colours
winston.addColors(customLevels);

const DEFAULT_METADATA = {};

/**
 * Creates a configured winston.Logger instance
 * @param {Object} [meta={}] Default metadata for each message logged
 * @return {winston.Logger}
 */
const createInstance = (meta = {}) => {
  const defaultParms = {
    level: process.env.LOG_LEVEL || 'debug',
    levels: customLevels.levels,
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.align(),
        winston.format.simple()
    ),
    colorize: true,
    transports: [
      new winston.transports.Console(),
    ],
    defaultMeta: Object.assign({}, DEFAULT_METADATA, meta),
  };

  if (process.env.LOG_ENABLE === 'false') {
    return winston.createLogger(Object.assign(defaultParms, {
      transports: [
        new DummyTransport(),
      ],
    }));
  }

  if (process.env.NODE_ENV && process.env.NODE_ENV.match(/^prod(uction)?$/)) {
    return winston.createLogger(Object.assign(defaultParms, {
      level: process.env.LOG_LEVEL || 'info',
      format: winston.format.json(),
      colorize: false,
    }));
  }

  return winston.createLogger(defaultParms);
};

module.exports = {
  DEFAULT_METADATA,
  createInstance,
};
