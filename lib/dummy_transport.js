const {Transport} = require('winston');

/**
 * Doesn't log anything. Useful for test suites to disable logging.
 * @type {DummyTransport}
 */
module.exports = class DummyTransport extends Transport {
  // eslint-disable-next-line require-jsdoc
  constructor() {
    super();
  }

  // eslint-disable-next-line require-jsdoc
  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });

    callback();
  }
};
