const chai = require('chai');
const {expect} = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const util = require('util');
const proxyquire = require('proxyquire');

const loggerClass = require('../');
const logger = loggerClass.createInstance();
const EsLogger = proxyquire('../es', {
  '.': {
    createInstance: () => logger,
  },
});

describe('EsLogger', () => {
  describe('createConstructor', () => {
    beforeEach(() => {
      sinon.stub(logger, 'error');
      sinon.stub(logger, 'warn');
      sinon.stub(logger, 'info');
      sinon.stub(logger, 'debug');
      sinon.stub(logger, 'trace');
    });

    afterEach(() => {
      logger.error.restore();
      logger.warn.restore();
      logger.info.restore();
      logger.debug.restore();
      logger.trace.restore();
    });

    describe('an EsLogger instance', () => {
      before(() => {
        const Logger = EsLogger.createConstructor();
        this.esLoggerInstance = new Logger();
      });

      describe('#error', () => {
        it('calls the winston logger with an error and context if passed an error', () => {
          const e = new Error('oh no');
          this.esLoggerInstance.error(e);
          expect(logger.error).to.have.been.calledWith(e);
        });

        it('calls the winston logger with a single line string and context', () => {
          this.esLoggerInstance.error('hello\nworld');
          expect(logger.error).to.have.been.calledWith('hello world');
        });

        describe('when it\'s a message telling us about a retry', () => {
          it('should redirect to warn', () => {
            this.esLoggerInstance.error('Request error, retrying POST https://...');
            expect(logger.warn).to.have.been.calledWith('Request error, retrying POST https://...');
            this.esLoggerInstance.error({message: 'Request error, retrying POST https://...'});
            expect(logger.warn).to.have.been.calledWith('{ message: \'Request error, retrying POST https://...\' }');
            const e = new Error('Request error, retrying POST https://...');
            this.esLoggerInstance.error(e);
            expect(logger.warn).to.have.been.calledWithMatch('Error: Request error, retrying POST https://...');
          });
        });
      });

      describe('#warning', () => {
        it('calls the winston logger with all arguments stringified and joined', () => {
          this.esLoggerInstance.warning('hello', 'world');
          expect(logger.warn).to.have.been.calledWith('hello world');
          this.esLoggerInstance.warning('hello', 'world', {a: 'foo'});
          expect(logger.warn).to.have.been.calledWith('hello world { a: \'foo\' }');
        });
      });

      describe('#info', () => {
        it('calls the winston logger with all arguments stringified and joined', () => {
          this.esLoggerInstance.info('hello', 'world');
          expect(logger.info).to.have.been.calledWith('hello world');
          this.esLoggerInstance.info('hello', 'world', {a: 'foo'});
          expect(logger.info).to.have.been.calledWith('hello world { a: \'foo\' }');
        });
      });

      describe('#debug', () => {
        it('calls the winston logger with all arguments stringified and joined', () => {
          this.esLoggerInstance.debug('hello', 'world');
          expect(logger.debug).to.have.been.calledWith('hello world');
          this.esLoggerInstance.debug('hello', 'world', {a: 'foo'});
          expect(logger.debug).to.have.been.calledWith('hello world { a: \'foo\' }');
        });
      });

      describe('#trace', () => {
        it('calls the winston logger with all params, stringified to remove line breaks', () => {
          this.esLoggerInstance.trace('POST', 'www.foo.com', {a: 'bar'}, {b: 'foo'}, 200);
          const expectedStr = util.inspect({
            method: 'POST',
            requestUrl: 'www.foo.com',
            body: {a: 'bar'},
            responseBody: {b: 'foo'},
            responseStatus: 200,
          }, {breakLength: Infinity});
          expect(logger.trace).to.have.been.calledWith(expectedStr);
        });
      });
    });
  });
});
