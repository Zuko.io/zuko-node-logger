const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const winston = require('winston');

const logger = require('../');

describe('logger', () => {
  beforeEach(() => {
    sinon.spy(winston, 'createLogger');
  });

  afterEach(() => {
    winston.createLogger.restore();
  });

  describe('when NODE_ENV=production', () => {
    before(() => {
      process.env.NODE_ENV = 'production';
    });

    after(() => {
      delete process.env.NODE_ENV;
    });

    it('should create a logger with a default log level of info', () => {
      logger.createInstance();
      expect(winston.createLogger).to.have.been.calledWithMatch({
        level: 'info',
      });
    });

    it('should create a logger that uses a JSON format output', () => {
      logger.createInstance();
      expect(winston.createLogger).to.have.been.calledWithMatch({
        format: winston.format.json(),
      });
    });
  });

  describe('when NODE_ENV=prod', () => {
    before(() => {
      process.env.NODE_ENV = 'prod';
    });

    after(() => {
      delete process.env.NODE_ENV;
    });

    it('should create a logger with a default log level of info', () => {
      logger.createInstance();
      expect(winston.createLogger).to.have.been.calledWithMatch({
        level: 'info',
      });
    });

    it('should create a logger that uses a JSON format output', () => {
      logger.createInstance();
      expect(winston.createLogger).to.have.been.calledWithMatch({
        format: winston.format.json(),
      });
    });
  });

  it('should create a logger with a log level of debug', () => {
    logger.createInstance();
    expect(winston.createLogger).to.have.been.calledWithMatch({
      level: 'debug',
    });
  });

  describe('when default metadata is provided', () => {
    beforeEach(() => {
      logger.DEFAULT_METADATA.requestId = 'abc-123';
    });

    it('should pass this on to winston', () => {
      logger.createInstance();
      expect(winston.createLogger).to.have.been.calledWithMatch({
        defaultMeta: {requestId: 'abc-123'},
      });
    });

    describe('when instance metadata is provided', () => {
      it('should pass this on to winston, overriding the default metadata', () => {
        logger.createInstance({requestId: 'xyz-321'});
        expect(winston.createLogger).to.have.been.calledWithMatch({
          defaultMeta: {requestId: 'xyz-321'},
        });
      });
    });
  });

  describe('when instance metadata is provided', () => {
    it('should pass this on to winston', () => {
      logger.createInstance({requestId: 'abc-123'});
      expect(winston.createLogger).to.have.been.calledWithMatch({
        defaultMeta: {requestId: 'abc-123'},
      });
    });
  });
});
